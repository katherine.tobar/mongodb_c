﻿namespace MongoDB
{
    public class AutorModel {
        public string country { get; set; }
        public int birthYear { get; set; }
    }
}
