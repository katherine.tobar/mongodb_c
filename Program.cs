﻿using System;

namespace MongoDB
{
    class Program
    {
        static void Main(string[] args)
        {
            MongoCRUD db = new MongoCRUD("libraryC");

            //INSERTAR DATOS

            //BookModel book = new BookModel
            //{
            //    title = "Libro 3",
            //    description = "Descripcion 3",
            //    author = "Author 3",
            //    pages = 218,
            //    AuthorDetails = new AutorModel
            //    {
            //        country = "Germany",
            //        birthYear = 1896
            //    }

            //};

            //db.InsertRecord("booksC", book);

            //SELECCIONAR TODOS LOS DATOS DE LA COLECCION
            var recs = db.LoadRecords<BookModel>("books");

            //foreach (var rec in recs)
            //{
            //    Console.WriteLine($"{rec.Id}: {rec.title}");

            //    if (rec.AuthorDetails != null)
            //    {
            //        Console.WriteLine(rec.AuthorDetails.country);
            //    }

            //}

            //UPSERT DATOS

            //var record = db.LoadRecordById<BookModel>("booksC", new Guid("14acc150-59a5-4d90-970e-f8900d9bc36b"));
            //record.PublicationDate = new DateTime(2005, 10, 31, 0, 0, 0, DateTimeKind.Utc);
            //db.UpserRecord("Books", record.Id, record);

            //ELIMINAR DATOS

            //db.DeleteRecord<BookModel>("Books", record.Id);
            Console.ReadLine();
        }
    }
}
