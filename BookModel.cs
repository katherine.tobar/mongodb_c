﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace MongoDB
{
    public class BookModel {
        [BsonId] //_id
        public Guid Id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string author { get; set; }
        public int pages { get; set; }
        public AutorModel AuthorDetails { get; set; }
        [BsonElement("pd")]
        public DateTime PublicationDate { get; set; }
    }
}
